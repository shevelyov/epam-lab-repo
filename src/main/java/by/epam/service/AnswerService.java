package by.epam.service;

import by.epam.entity.Answer;

import java.util.List;

public interface AnswerService {
    public List<Answer> getAnswersByQuestionId(long id);
}
