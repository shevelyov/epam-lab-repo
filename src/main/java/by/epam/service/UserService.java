package by.epam.service;

import by.epam.entity.User;

public interface UserService {
    public User getUserByLogin(String login);
    public User getUserByEmail(String email);

}
