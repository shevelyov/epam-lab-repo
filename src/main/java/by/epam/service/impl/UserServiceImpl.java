package by.epam.service.impl;

import by.epam.dao.UserRepository;
import by.epam.entity.User;
import by.epam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User getUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.findByLogin(email);
    }

    public void register(User user){
        userRepository.save(user);
    }

    public User find(long id){
        return userRepository.findOne(id);
    }

    public List<User> findAll(){
        return (List<User>) userRepository.findAll();
    }

    public void delete(long id) {
        userRepository.delete(id);
    }
}
