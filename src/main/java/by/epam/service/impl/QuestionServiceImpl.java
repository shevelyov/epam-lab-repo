package by.epam.service.impl;

import by.epam.dao.QuestionRepository;
import by.epam.entity.Question;
import by.epam.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    QuestionRepository questionRepository;

    @Override
    public List<Question> getQuestionsByTheme(String theme) {
        return questionRepository.findQuestionByTheme(theme);
    }

    public Question find(long id){
        return (Question) questionRepository.findOne(id);
    }

    public List<Question> findAll(){
        return (List<Question>) questionRepository.findAll();
    }

    public void register(Question question) { questionRepository.save(question); }

    public void delete(long id) { questionRepository.delete(id); }
}
