package by.epam.service.impl;

import by.epam.dao.AnswerRepository;
import by.epam.entity.Answer;
import by.epam.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerServiceImpl implements AnswerService{

    @Autowired
    AnswerRepository answerRepository;

    @Override
    public List<Answer> getAnswersByQuestionId(long id) {
        return answerRepository.findAnswersByQuestionId(id);
    }

    public Answer find(long id){
        return (Answer) answerRepository.findOne(id);
    }

    public List<Answer> findAll(){
        return (List<Answer>) answerRepository.findAll();
    }

    public void register(Answer answer) { answerRepository.save(answer); }

    public void delete(long id) { answerRepository.delete(id); }
}
