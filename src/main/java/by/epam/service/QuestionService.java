package by.epam.service;


import by.epam.entity.Question;

import java.util.List;

public interface QuestionService {
    public List<Question> getQuestionsByTheme(String theme);
}
