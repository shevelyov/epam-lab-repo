package by.epam.controller;

import by.epam.entity.User;
import by.epam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

    @Autowired
    UserService userService;
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("user", new User());
        return "login";
    }
    @RequestMapping(value = "/loginProcess", method = RequestMethod.POST)
    public String loginProcess(@ModelAttribute("login") String login, Model model) {
        User user = userService.getUserByLogin(login);
        if (null != user) {
            model.addAttribute("name",user.getName());
            return "welcome";
        } else {
            model.addAttribute("message", "Username or Password is wrong!!");
            return "login";
        }
    }

}
