package by.epam.controller;


import by.epam.service.impl.AnswerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/questions/*/answers")
public class AnswerController {
    @Autowired
    private AnswerServiceImpl answerService;

    @RequestMapping(value="/{id}/answers",method = RequestMethod.GET)
    public String getAllAnswersByQuestion(@PathVariable("id") long id, Model model){
        model.addAttribute("answers",answerService.getAnswersByQuestionId(id));
        return "answers/view";
    }
}
