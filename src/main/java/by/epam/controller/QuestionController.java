package by.epam.controller;

import by.epam.entity.Question;
import by.epam.service.impl.QuestionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@RequestMapping("/question")
public class QuestionController {
    @Autowired
    private QuestionServiceImpl questionService;

    @RequestMapping(method = RequestMethod.GET)
    public String getAllQuestions(Model model){
        model.addAttribute("questions",questionService.findAll());
        return "questions";
    }

    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public String getQuestion(@PathVariable("id") long id, Model model) {
        model.addAttribute("question",questionService.find(id));
        return "view";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void createQuestion(@PathVariable("id") long id, Question question) {
        questionService.register(question);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteQuestion(@PathVariable("id") long id) {
        questionService.delete(id);
    }

    @RequestMapping(method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Question createQuestion(Question question, BindingResult result, HttpServletResponse response) throws BindException {
        if (result.hasErrors()) {
            throw new BindException(result);
        }
        questionService.register(question);
        response.setHeader("Location", "/questions/" + question.getId());
        return question;
    }
}
