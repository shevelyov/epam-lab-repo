package by.epam.dao;

import by.epam.entity.Question;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface QuestionRepository extends CrudRepository<Question, Long> {
    public List<Question> findQuestionByTheme(String theme);
}
