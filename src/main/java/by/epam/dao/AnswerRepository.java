package by.epam.dao;

import by.epam.entity.Answer;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface AnswerRepository extends CrudRepository<Answer, Long> {
    public List<Answer> findAnswersByQuestionId(long id);
}
