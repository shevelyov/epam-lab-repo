package by.epam.dao;

import by.epam.entity.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface UserRepository extends CrudRepository<User, Long>{
    public User findByLogin(String login);
    public User findByEmail(String email);
}
